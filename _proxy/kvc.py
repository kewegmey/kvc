# Kory Wegmeyer
# kory334@gmail.com
# 9/12/19
# Salt vCenter proxy minion: Kory vCenter (KVC)

from __future__ import absolute_import, print_function, unicode_literals
import salt.exceptions
import logging
import time
import ssl
import platform

# Set up logging
log = logging.getLogger(__name__)

PROXY_CONFIG = {}

try:
    from pyVmomi import vim
    from pyVim import connect
    has_pyvmomi = True
except ImportError:
    log.debug("#######################")
    log.error("Failed to load pyVmomi!")
    log.debug("#######################")
    has_pyvmomi = False

__proxyenabled__ = ['kvc']
#log.debug("About to clear data")
#DETAILS = {}
#log.debug("data cleared")
is_init = False


def __virtual__():
    log.debug("#######################")
    log.debug("kvc proxy __virtual__() called")
    log.debug(platform.python_version())
    log.debug("#######################")
    if has_pyvmomi:
        return True
    else:
        return False

def init(opts=None):
 #   global PROXY_CONFIG
 #   global si
    log.debug("#######################")
    log.debug("kvc proxy init() called")
    log.debug("#######################")
    try:
        disconnect_si(get_si())
    except Exception as e:
        log.error(e)
        return False
    is_init = True

def initialized():
    # Check service instance is connected
    log.debug("#######################")
    log.debug("kvc proxy initalized() called")
    log.debug("#######################")
    return is_init

def shutdown(opts):
    log.debug("#######################")
    log.debug("kvc proxy shutdown() called")
    log.debug("#######################")

def ping():
    log.debug("#######################")
    log.debug("kvc proxy ping() called")
    log.debug("#######################")

    try:
        si = get_si()
        log.debug(si.content.rootFolder.childEntity)
        disconnect_si(si)
    except Exception as e:
        log.error(e)
        return False
    return True

def alive(opts=None):
    log.debug("#######################")
    log.debug("kvc proxy alive() called")
    log.debug("#######################")
    try:
        si = get_si()
        log.debug(si.content.rootFolder.childEntity)
        disconnect_si(si)
    except Exception as e:
        log.error(e)
        return False
    return True

def grains():
    log.debug("#######################")
    log.debug("kvc proxy grains() called")
    log.debug("#######################")
    return {'korygrain':'fromproxy'}

def grains_refresh():
    log.debug("#######################")
    log.debug("kvc proxy grains_refresh() called")
    log.debug("#######################")
    return {'korygrain':'fromproxy'}

def get_si():
    log.debug("#######################")
    log.debug("kvc proxy get_si() called")
    log.debug("#######################")
    try:
        DETAILS = {} 
        DETAILS['config'] = __pillar__.get('proxy', {})
        context = ssl._create_unverified_context()
        si = connect.Connect(host=DETAILS['config']['vcenter'], user=DETAILS['config']['username'], pwd=DETAILS['config']['password'], sslContext=context)
    except Exception as e:
        log.error(e)
        return False
    return si

def disconnect_si(si):
    log.debug("#######################")
    log.debug("kvc proxy disconnect_si() called")
    log.debug("#######################")
    connect.Disconnect(si)

def _search(si,vimtype, kw=None):
    '''
    Uses the view manager to search for objects.
    returns a list of the results
    '''
    out = []
    view = si.content.viewManager.CreateContainerView(container=si.content.rootFolder, type=[vimtype], recursive=True)
    # If no KW specified just return everything that matches.
    if kw != None:
        for obj in view.view:
            if kw in obj.name:
                out.append(obj)
        view.DestroyView()
        return out
    else:
        return view.view
#################################################
# Begin functions called from execution modules #
#################################################

def get_datacenters(kw=None):
    dcs = []
    si = get_si()
    dcso = _search(si, vim.Datacenter, kw)
    for dc in dcso:
        dcs.append(dc.name)
    ret = {'Datacenters': dcs}
    disconnect_si(si)
    return ret

def create_datacenter(dcname=None, foldername=None):
    '''
    Created datacenter in root folder.
    '''
    try:
        si = get_si()
        folder = None

        if len(dcname) > 79:
            raise ValueError("The name of the datacenter must be under "
                         "80 characters.")
        if folder is None:
            folder = si.content.rootFolder

        if folder is not None and isinstance(folder, vim.Folder):
                dc_moref = folder.CreateDatacenter(name=dcname)
        toreturn = dc_moref.name
    except vim.fault.DuplicateName as e:
	log.error("Duplicate datacenter name!!!!!")
        disconnect_si(si)
        nice_error = {"error": {"msg": e.msg, "conflicting_object": str(e.object)}}
        return nice_error
    else:
        disconnect_si(si)
        return toreturn

def destroy_datacenter(dcname):
    '''
    Reove DC matching name provided.
    Are Datacenters named unique?
    '''
    si = get_si()
    dcs = _search(si, vim.Datacenter, dcname)
    if len(dcs) > 1:
        disconnect_si(si)
        return "Too many matches!"
    if len(dcs) == 0:
        disconnect_si(si)
        return "No datacenter by name " + dcname + " found." 
    name = dcs[0].name
    task = dcs[0].Destroy_Task()
    while task.info.state != 'success':
        time.sleep(1)    
    disconnect_si(si)
    return {'Destroyed datacenter': name}

def get_clusters(kw=None):
    cs = []
    si = get_si()
    clusters = _search(si, vim.ClusterComputeResource, kw)
    for c in clusters:
        cs.append(c.name)
    disconnect_si(si)
    return cs

def add_cluster_host(hostname, username, password, clustername):
    # Find cluster specified
    si = get_si()
    cluster = _search(si, vim.ClusterComputeResource, clustername)
    if len(cluster) > 1:
        disconnect_si(si)
        return "Specified cluster matched more than one object!"
    if len(cluster) == 0:
        disconnect_si(si)
        return "No cluster by name " + clustername + " found." 
    #vcconf = __pillar__['vcconf']
    conspec = vim.host.ConnectSpec()
    conspec.hostName = hostname
    conspec.userName = username
    conspec.password = password
    log.debug(str(conspec))
    task = cluster[0].AddHost_Task(spec=conspec, asConnected=True)
    while True:
        if task.info.state == 'error':
            errstr = "Task failed with error: " + str(task.info.error)
            log.error(errstr)
            disconnect_si(si)
            return errstr
        elif task.info.state == 'success':
            disconnect_si(si)
            return hostname
        elif task.info.state == 'running':
            log.debug("Task add host " + hostname + " to cluster " + clustername + " running.")
        time.sleep(.5)
    disconnect_si(si)
    # Need more hosts to fully test.  Error handling seems to work well.
    return True

def get_cluster_host(clustername):
    si = get_si()
    cluster = _search(si, vim.ClusterComputeResource, clustername)
    if len(cluster) > 1:
        disconnect_si(si)
        return "Specified cluster matched more than one object!"
    if len(cluster) == 0:
        disconnect_si(si)
        return "No cluster by name " + clustername + " found."
    hosts = []
    for host in cluster[0].host:
        hosts.append(host.name) 
    disconnect_si(si)
    return hosts


