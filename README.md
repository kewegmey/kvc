# Salt Modules for vCenter

This is the start of a new set of salt modules for vCenter.

The first phase of development will focus on creating a proxy module that implements major vSphere management features.  For now, all the "brains" live in the proxy module.  The execution module is just a wrapper.
After that, state modules will be developed.  

# To Do
*  Proxy/execution
    *  Host Management
        *  ~~finish and test add_cluster_host()~~
        *  Add mtce(): toggles maintenance mode
        *  Add get_mtce(): gets maintenance mode state
        *  Add set_lic(): set license key
        *  Add get_lic(): gets current license key
        *  Add add_dvs_host(): adds host to DVS
        *  Add get_dvs_host(): gets host member DVSs
        *  Add add_virt_nic(): adds a virtual network adapter to a host
        *  Add get_virt_nic(): gets host virtual network adapters
        *  Add set_host_prop: sets host advanced property
        *  Add get_host_prop: gets a host advanced property
        *  Add mount_nfs: mounts NFS datastore
        *  Add get_nfs:  gets mounted NFS datastores
        *  Add set_ntp:  sets ntp settings
        *  Add get_ntp:  gets ntp settings/state
        *  Add set_vmswap
        *  Add get_vmswap
        *  Add enable_fw_rule:  Enable firewall rule
        *  Add get_fw_rule:  Gets status of firewall rules
        *  Add reboot_host
        *  Add/remove/get permissions on hosts
        *  Add delete/remove fuctions for most of the above
        *  Probably more things I forgot
    * DVS Management
        *  Add create_dvs()
        *  Functions for any settings we want to enforce on the switches. LAG config?
        *  Add create_dvportgroup()
        *  Add get_dvportgroup()
    * vCenter Administration
        *  Roles
        *  Global Perms
        *  Licenses
        *  SSO Users and Groups
        *  SSO Configuration
    * VM Management
        *  Add/remove/get permissions on VMs and folders
        *  set/get vm folder for VM
        *  This gets into salt-cloud territory...
    * Storage Management
        *  set/get all datastore settings (SIOC, ?)
        *  add/remove/get permissions on datastores
        *  add/remove/get datastore clusters
        *  set/get datastore cluster settings 
*  State
    *  Matching states for all execution functions.
*  Other
    *  Figure out sensible yaml pillar schema to feed states


# Pillar
In addition to the proxy config pillar I expect pillar to be used. The states will be templated quite heavily.
Here is an idea for a start of the schema:

```
vcconf:
  host:
  - name: 192.168.1.1
    dslist: one
    network_template: nettemp1
    property_template: proptemp1
    vm_swap: '[test] scratch/swap'
    vnic:
    - ip: ''
      netmask: ''
      vmotion: false
    - ip: ''
      netmask: ''
      vmotion: false
  dslists:
    one:
    - ds1
    - ds2
    - ds3
    two:
    - ds1
    - ds3
  nettemps:
    nettemp1:
      switch:
        vm:
          uplinks: []
          switch_name: ''
        vmotion:
          uplinks: []
          switch_name: ''
        storage:
          uplinks: []
          switch_name: ''
  proptemps:
    proptemp1:
      NFS.MaxVol: 256
```
A possibly better way is to use salt.pillar.stack to load pillar in a specific order to apply a general pillar to all hosts then more specific for host groups, etc.
