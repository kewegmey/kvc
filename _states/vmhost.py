import salt.exceptions
import logging

log= logging.getLogger(__name__)

def exists(name, username, password, cluster=False):
    '''
    Ensures a ESXi host is added to the desired container.

    name
        The host to operate on.
    username
        The host username used to add the host if needed.
    password
        The host password used to add the host if needed.
    cluster
        optional, use if you want to ensure host is a cluster member.
    '''
    ret = {
        'name': name,
        'changes': {},
        'result': False,
        'comment': '',  
    }
    if cluster:
        # Check if we're in the desired state already.
        curr = __salt__['kvc.get_cluster_host'](cluster)
        log.debug(str(curr))
        if name in curr:
            ret['result'] = True
            ret['comment'] = 'Host ' + name + ' already a member of cluster ' + cluster + '.'
            return ret
        else:
            # Check if running in test mode
            if __opts__['test']:
                newhosts = curr[:]
                newhosts.append(name)
                ret['comment'] = 'Would add host ' + name + ' to cluster ' + cluster + '.'
                ret['changes'] = {
                    'old': curr,
                    'new': newhosts 
                }
                ret['result'] = None
                return ret
            else:
                #Make change
                out = __salt__['kvc.add_cluster_host'](name, username, password, cluster)
                if out == name:
                    ret['comment'] = 'Added host ' + name + ' to cluster ' + cluster + '.'
                    ret['changes'] = {
                        'old': curr,
                        'new': __salt__['kvc.get_cluster_host'](cluster)
                    }
                    ret['result'] = True
                else:
                    ret = {
                        'name': name,
                        'changes': {},
                        'result': False,
                        'comment': out,  
                    }
                    return ret         
    return ret   
