# Import Python libs
from __future__ import absolute_import, print_function, unicode_literals
import logging

# Import Salt libs
import salt.utils.platform


log = logging.getLogger(__name__)

__proxyenabled__ = ['kvc']
__virtualname__ = 'kvc'


def __virtual__():
    '''
    Only work on proxy
    '''
    if salt.utils.platform.is_proxy():
        return __virtualname__
    return (False, 'The kvc execution module failed to load: '
            'only available on proxy minions.')


def get_datacenters(kw=None):
    if kw == None:
        return __proxy__['kvc.get_datacenters']()
    else:
        return __proxy__['kvc.get_datacenters'](kw)
def create_datacenter(*args):
    return __proxy__['kvc.create_datacenter'](*args)
def destroy_datacenter(*args):
    return __proxy__['kvc.destroy_datacenter'](*args)
def get_clusters(*args):
    return __proxy__['kvc.get_clusters'](*args)
def add_cluster_host(*args):
    return __proxy__['kvc.add_cluster_host'](*args)
def get_cluster_host(*args):
    return __proxy__['kvc.get_cluster_host'](*args)
